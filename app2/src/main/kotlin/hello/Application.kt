package hello

import org.slf4j.LoggerFactory
import org.springframework.amqp.core.BindingBuilder
import org.springframework.amqp.core.FanoutExchange
import org.springframework.amqp.core.Queue
import org.springframework.amqp.rabbit.annotation.RabbitListener
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.annotation.Bean
import org.springframework.stereotype.Component
import java.util.concurrent.CountDownLatch

/**
 * @author sala
 */
fun main(args: Array<String>) {
    SpringApplication.run(Configuration::class.java, *args)
}


@SpringBootApplication
open class Configuration {
    companion object {
        val QUEUE_NAME = "spring-boot"
        val EXCHANGE_NAME = "spring-boot-exchange"

    }

    @Bean open fun queue() = Queue(QUEUE_NAME, false)

    @Bean open fun queue2() = Queue("spring-boot2", false)

    @Bean open fun exchange() = FanoutExchange(EXCHANGE_NAME)

    @Bean open fun binding(queue: Queue, exchange: FanoutExchange) = BindingBuilder.bind(queue).to(exchange)

    @Bean open fun binding2(queue2: Queue, exchange: FanoutExchange) = BindingBuilder.bind(queue2).to(exchange)
}

@Component
class Receiver {
    companion object {
        val logger = LoggerFactory.getLogger(Receiver::class.java)
    }


    @RabbitListener(queues = arrayOf("spring-boot"))
    fun receiveMessage(message: String) {
        logger.info("Received <$message>")
    }

    @RabbitListener(queues = arrayOf("spring-boot2"))
    fun receiveMessage2(message: String) {
        logger.info("Received 2 <$message>")
    }
}