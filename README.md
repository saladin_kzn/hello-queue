## Simple docker-compose + rabbit-mq + spring boot app

Example app for demonstrating docker-compose features.

### How to run?

It's easy ;-)
```
	./gradlew distTar && docker-compose build && docker-compose up
```