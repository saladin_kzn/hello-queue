package hello

import org.slf4j.LoggerFactory
import org.springframework.amqp.core.FanoutExchange
import org.springframework.amqp.rabbit.core.RabbitTemplate
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.annotation.Bean
import org.springframework.scheduling.annotation.EnableScheduling
import org.springframework.scheduling.annotation.Scheduled

/**
 * @author sala
 */
class Application {
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            SpringApplication.run(hello.Configuration::class.java, *args)
        }
    }
}

@SpringBootApplication
@EnableScheduling
open class Configuration {
    companion object {
        val EXCHANGE_NAME = "spring-boot-exchange"

        val logger = LoggerFactory.getLogger(Application::class.java)
    }

    @Scheduled(fixedRate = 5000)
    fun run() {
        logger.info("Sending message...");
        rabbitTemplate.convertAndSend(EXCHANGE_NAME, "", "Hello from RabbitMQ!");
    }

    @Autowired
    lateinit var rabbitTemplate: RabbitTemplate

    @Bean open fun exchange() = FanoutExchange(EXCHANGE_NAME)
}